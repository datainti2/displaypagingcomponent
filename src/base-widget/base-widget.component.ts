import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-displaypaging',
  templateUrl: './base-widget.component.html',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetComponent implements OnInit {
public DisplayPaging: Array<any> = [
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	},
	{
		"display_name": "test insert topic",
		"counter": "234"
	}
];
  constructor() { }

  ngOnInit() {
  }

}
